package Album;

import Anotation.AlbumData;

public class Album {

    @AlbumData(artistName = "Kendrick Lamar")
    private int anInt;
    @AlbumData
    private String artistName;
    private String albumName;
    private int year;

    @Override
    public String toString() {
        return "Artist : " + artistName + "\nAlbum : "
                + albumName + "\nYear : " + year;
    }

    private void sayHello() {
        System.out.println("Hello reflection!!!");
    }

    private void myMethod(String s, int... args) {
        String newS = s.toUpperCase();
        int sum = 0;
        for (int i :
                args) {
            sum += i;
        }
        System.out.println("New string : " + newS
                + "\n" + "Sum of all elements : " + sum);
    }

    private void myMethod(String... args) {
        StringBuilder str = new StringBuilder();
        for (String s :
                args) {
            str.append(s);
        }
        System.out.println(str.reverse());
    }

    private void test(String str) {
        System.out.println("Hello " + str + "!!");
    }

    public int getAnInt() {
        return anInt;
    }

    public void setAnInt(int anInt) {
        this.anInt = anInt;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getAlbumName() {
        return albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
