import Album.Album;
import DataGetter.*;
import MyClass.MyClass;

import java.lang.reflect.InvocationTargetException;

public class Main {
    public static void main(String[] args) {
        Album album = new Album();
        System.out.println(album);
        try {
            DataGetter.getData(album);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        Invoker.invokeMethod(album,"sayHello");
        System.out.println("///////////////////////My methods/////////////////");
        Invoker.invokeMyMethods(album);

        System.out.println("//////////////////////////////////////////////////");
        new MyClass<Album>(Album.class);
    }
}
