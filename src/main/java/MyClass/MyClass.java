package MyClass;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class MyClass<T> {

    private T t;
    private Class<T> gClass;

    public MyClass(Class<T> gClass) {
        this.gClass = gClass;

        try {
            t = gClass.getConstructor().newInstance();
            System.out.println("The declared fields of " + gClass.getSimpleName() + " are : ");
            Field[] fields = gClass.getDeclaredFields();
            for (Field f:
                 fields) {
                System.out.println(f.getName());
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

}
