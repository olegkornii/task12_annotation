package DataGetter;

import Anotation.AlbumData;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class DataGetter {

    public static void getData(Object o) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        Class oClass = o.getClass();

        showClassAndConstructor(oClass);
        showFields(o);
        showMethods(oClass);
        showAnnotation(oClass);

    }

    private static void showFields(Object o) throws IllegalAccessException {

        Class oClass = o.getClass();
        System.out.println("Declared fields : ");
        Field[] fields = oClass.getDeclaredFields();
        for (Field f :
                fields) {
            System.out.println(f.getName());
            if (f.isAnnotationPresent(AlbumData.class)) {
                AlbumData albumData = f.getAnnotation(AlbumData.class);
                System.out.println("    name in Annotation : " + albumData.artistName());

            }
        }
        fields[3].setAccessible(true);
        if (fields[3].getType().equals(int.class)) {
            fields[3].setInt(o, 111);
        }
    }

    private static void showMethods(Class oClass){
        System.out.println("The declared methods : ");
        Method[] methods = oClass.getDeclaredMethods();
        for (Method m :
                methods) {
            System.out.println(m.getName());
        }
    }

    private static void showClassAndConstructor(Class oClass) throws NoSuchMethodException {
        System.out.println("The name of class : " + oClass.getSimpleName());

        Constructor oConstructor = oClass.getConstructor();
        System.out.println("The name of constructor : " + oConstructor.getName());
    }

    private static void showAnnotation(Class oClass){
        Annotation annotationF = oClass.getAnnotation(AlbumData.class);
        System.out.println(annotationF);
    }


}
