package DataGetter;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Invoker {

    public static void invokeMethod(Object o, String mName)  {

        try {

            Class oClass = o.getClass();
            Method m = oClass.getDeclaredMethod(mName);
            m.setAccessible(true);
            m.invoke(o);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

    }

    public static void invokeMyMethods(Object o) {
        Class oClass = o.getClass();

        try {
            Method myM1 = oClass.getDeclaredMethod("myMethod", String[].class);
            myM1.setAccessible(true);
            myM1.invoke(o, (Object) new String[]{"Hello ", "reflection ", "you ", "are ", "really ", "interesting "});

            Method myM2 = oClass.getDeclaredMethod("myMethod", String.class, int[].class);
            myM2.setAccessible(true);
            myM2.invoke(o, "Hello", new int[]{2, 3, 45, 12, 54});

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
