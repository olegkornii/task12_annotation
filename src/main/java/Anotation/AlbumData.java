package Anotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value = ElementType.FIELD)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface AlbumData {
    String artistName() default "Kanye West";
    String albumName() default "The life of Pablo";
    int year() default 2015;

}
